import {View, Text, TextInput} from 'react-native'
import React from 'react'
import Icon from 'react-native-vector-icons/Ionicons'
export default function Contactus() {
    return (
      <View style={{ flex: 1, margin:30}}>
        <Text style={{fontSize:26,fontWeight:'bold', marginBottom:10}}>Hotline</Text>
        <View style={{flexDirection:'row'}}>
       <Icon name='call' style={{color:'red'}} size={24}/>
        <Text style={{fontSize:24,fontWeight:'bold'}}>*7777</Text>
        </View>


        <View style={{flexDirection:'row'}}>
       <Icon name='call' style={{color:'green'}} size={24}/>
        <Text style={{fontSize:24,fontWeight:'bold'}}>+994 55 3945917</Text>
        </View>
        <Text style={{fontSize:26,fontWeight:'bold', marginTop:10, marginBottom:10}}> Contact us</Text>
        <View style={{flexDirection:'row'}}>
        <Text style={{fontSize:20}}>info@kernel.com </Text>
        <Icon name='mail' style={{color:'red'}} size={24}/>

        </View>
        <View style={{flex:1,flexDirection:'row'}}>
        <Icon name='call' style={{color:'green'}} size={24}/>
        <Text style={{fontSize:24,fontWeight:'bold'}}>(012) 212 2565</Text>

        </View>
        
      </View>
    );
  }