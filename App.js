import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'
import Icon from 'react-native-vector-icons/Ionicons'
import {createStackNavigator} from '@react-navigation/stack'
import {createDrawerNavigator} from '@react-navigation/drawer'
import React, {useState, useEffect} from 'react'
import {View,Text } from 'react-native'
import Basket from './Basket'
import Menu from './Menu'
import Restaurant from './Restaurant'
import Notifications from './Notifications'
import Faq from './Faq'
import Contactus from './Contactus';
import {DrawerContent} from './DrawerContent'
import basketim, {nStatus} from './fancyClass'





const Tab = createMaterialBottomTabNavigator()
const Stack = createStackNavigator()
const Drawer = createDrawerNavigator()

const MenuStack = ({navigation, ct, setCt, items, setItems}) => {
  
  return(
  <Stack.Navigator>
    <Stack.Screen name="Menu" options={{
          headerStyle: {
            backgroundColor: 'whitesmoke'
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            
          },
          headerLeft: () => (
            <View style={{flexDirection:'row'}}>
  
    <Icon
     
      onPress={() => navigation.toggleDrawer()}
     color="red"
     
        name="menu-outline"
        size={26}
        color="black"
        style={{marginStart:10}}
      
    />

    </View>
          ),
          
        }} >
          {props => <Menu {...props} items={items} setItems={setItems} ct={ct} setCt={setCt} />}
        </Stack.Screen>
  </Stack.Navigator>
  )
}
const BasketStack = ({navigation, ct, setCt, items}) => {
  return(
  <Stack.Navigator>
    <Stack.Screen name="Basket" options={{
          headerStyle: {
            backgroundColor: 'whitesmoke',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            
          },
          headerLeft: () => (
         <View style={{flexDirection:'row'}}>
                  
            <Icon
             
              onPress={() => {
                navigation.toggleDrawer()
                }}
             color="red"
             
                name="menu-outline"
                size={26}
                color="black"
                style={{marginStart:10}}
              
            />
      
            </View>
            
          ),
              }}> 
              {props => <Basket {...props} ct={ct} setCt={setCt} items={items} />}
              </Stack.Screen>
  </Stack.Navigator>
  )
}
const NotificationsStack = ({navigation, status}) => {
  return(
  <Stack.Navigator>
    <Stack.Screen name="Notifications" children={props => <Notifications {...props} status={status} />} options={{
          headerStyle: {
            backgroundColor: 'whitesmoke',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            
          },
          headerLeft: () => (
         <View style={{flexDirection:'row'}}>
                  
            <Icon
             
              onPress={() => navigation.toggleDrawer()}
             color="red"
             
                name="menu-outline"
                size={26}
                color="black"
                style={{marginStart:10}}
              
            />
      
            </View>
            
          ),
        }} />
  </Stack.Navigator>
  )
}
const FaqStack = ({navigation}) => {
  return(
  <Stack.Navigator>
    <Stack.Screen name="FAQ" component={Faq} options={{
          headerStyle: {
            backgroundColor: 'whitesmoke',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            
          },
          headerLeft: () => (
         <View style={{flexDirection:'row'}}>
                  
            <Icon
             
              onPress={() => navigation.toggleDrawer()}
             color="red"
             
                name="menu-outline"
                size={26}
                color="black"
                style={{marginStart:10}}
              
            />
      
            </View>
            
          ),
        }} />
  </Stack.Navigator>
  )
}
const ContactusStack = ({navigation}) => {
  return(
  <Stack.Navigator>
    <Stack.Screen name="Contact us" component={Contactus} options={{
          headerStyle: {
            backgroundColor: 'whitesmoke',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            
          },
          headerLeft: () => (
         <View style={{flexDirection:'row'}}>
                  
            <Icon
             
              onPress={() => navigation.toggleDrawer()}
             color="red"
             
                name="menu-outline"
                size={26}
                color="black"
                style={{marginStart:10}}
              
            />
      
            </View>
            
          ),
        }} />
  </Stack.Navigator>
  )
}

const ExitStack = ({navigation}) => {
  return(
  <Stack.Navigator>
    <Stack.Screen name="Exit" options={{
          headerStyle: {
            backgroundColor: 'whitesmoke',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            
          },
          headerLeft: () => (
         <View style={{flexDirection:'row'}}>
                  
            <Icon
             
              onPress={() => navigation.toggleDrawer()}
             color="red"
             
                name="menu-outline"
                size={26}
                color="black"
                style={{marginStart:10}}
              
            />
      
            </View>
            
          ),
        }} />
  </Stack.Navigator>
  )
}
const RestaurantStack = ({navigation}) => {
  return(
  <Stack.Navigator>
    <Stack.Screen name="Restaurant" component={Restaurant} options={{
          headerStyle: {
            backgroundColor: 'whitesmoke',
          },
          headerTintColor: 'black',
          headerTitleStyle: {
            
          },
          headerLeft: () => (
         <View style={{flexDirection:'row'}}>
                  
            <Icon
             
              onPress={() => navigation.toggleDrawer()}
             color="red"
             
                name="menu-outline"
                size={26}
                color="black"
                style={{marginStart:10}}
              
            />
      
            </View>
            
          ),
        }} />
  </Stack.Navigator>
  )
}


 function MyTabs() {
  const [items, setItems] = useState(basketim.basketItems())
  const [ct, setCt] = useState(1)
  const [data, setData] = useState([])
  const [st, setSt] = useState(1)
  const [status, setStatus] = useState(nStatus.statusMessage)
  
useEffect(()=> {
  
   setStatus(nStatus.statusMessage)
   console.log(status)
 }, [status, nStatus.statusMessage])
  useEffect(()=>{
    
    setItems(basketim.basketItems())
  
    
  }, 
  [ct, items, st, basketim.basketItems()])
  
 useEffect(() => {
  fetch('https://donerweb.herokuapp.com/wholemenus')
  .then(res => res.json())
  .then(res => 
   setData(res))
 }, [data])
 
  return (
    <Tab.Navigator
      initialRouteName="Menu"
      activeColor="red"
      labelStyle={{ fontSize: 12 }}
      barStyle={{ backgroundColor: 'whitesmoke' }}


    >
      <Tab.Screen
        name="Menu"
        options={{
          tabBarLabel: 'Menu',
          tabBarIcon: ({ color }) => (
            <Icon name="grid-outline" color={color} size={26} />
          ),
        }}
      >
        {props => <MenuStack {...props} items={data} ct={st} setCt={setSt} />}
        </Tab.Screen>
      <Tab.Screen
        name="Basket"
        options={{
          tabBarLabel: 'Basket',
          tabBarIcon: ({ color }) => (
           <View>
             {items.length !==0 ?
              <View >
              <View style={{position:'absolute', height:20, width:20, borderRadius:12,backgroundColor:'rgb(250,0,0)',
            left:18, bottom:18, alignItems:'center', justifyContent:'center', zIndex:99}}>
             
            
          <Text style={{color:'white', fontSize:11}}>{items.length}</Text>
             
              </View>
            <Icon name="basket-outline" color={color} size={26} />
            
            </View>: <Icon name="basket-outline" color={color} size={26} />}
           </View>
          ),
        }}
      > 
                    {props => <BasketStack {...props} setCt={setCt} ct={ct} items={items} />}

      </Tab.Screen>
       <Tab.Screen
        name="Notfications"
        children={props => <NotificationsStack {...props} status={status} />}
        options={{
          tabBarLabel: 'Notifications',
          tabBarIcon: ({ color }) => (
              <View>
                {status.length !==0 ?
                 <View >
                 <View style={{position:'absolute', height:20, width:20, borderRadius:12,backgroundColor:'rgb(250,0,0)',
               left:18, bottom:18, alignItems:'center', justifyContent:'center', zIndex:99}}>
                
               
             <Text style={{color:'white', fontSize:11}}>{status.length}</Text>
                
                 </View>
               <Icon name="notifications-outline" color={color} size={26} />
               
               </View>: 
            <Icon name="notifications-outline" color={color} size={26} />}
            </View>
          ),
                }}
      />
      <Tab.Screen
        name="Restaurant"
        component={RestaurantStack}
        options={{
          tabBarLabel: 'Restaurant',
          tabBarIcon: ({ color }) => (
            <Icon name="location-outline" color={color} size={26} />
          ),
        }}
      />
           
     

    </Tab.Navigator>
  );
}


export default function App() {
  return (
    <NavigationContainer>
        <Drawer.Navigator initialRouteName="Main" drawerType="back"
        drawerContent={props => <DrawerContent {...props} />}
        drawerContentOptions={{
        activeTintColor: 'black',
        inactiveBackgroundColor:'white',
        inactiveTintColor: 'black',
        activeBackgroundColor:'white',
        itemStyle: { marginVertical: 5, marginHorizontal:15},

      }}    
      >
        
  
        <Drawer.Screen name="Main"    component={MyTabs} />
        <Drawer.Screen name="FAQ"  component={FaqStack} />
        <Drawer.Screen name="Contact us"  component={ContactusStack} />
        <Drawer.Screen name="Exit"  component={ExitStack} />


      </Drawer.Navigator>
      

    </NavigationContainer>
  );
}