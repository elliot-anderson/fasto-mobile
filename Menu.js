import React from 'react';
import { StyleSheet, View, Text, Image, ScrollView, TouchableHighlight } from 'react-native';
import { SectionGrid } from 'react-native-super-grid';
import basketim from './fancyClass.js'
import { Button } from 'react-native-paper';


export default function Example({ct, setCt, items}) {

  return (
    <SectionGrid
      itemDimension={150}
      //staticDimension={200}
      // fixed
      spacing={10}
      sections={[
        {
          title: 'Dönərlər',
          data: items.filter(items => items.type ==='doner'),
        },
        {
          title: 'Şorba və Salat',
          data: items.filter(items => items.type ==='sorba_salat'),
        },
        {
          title: 'İçkilər',
          data: items.filter(items => items.type ==='icki'),
        },
      ]}
      style={styles.gridView}
      renderItem={({ item, section, index }) => (
        <ScrollView>
        <View style={[styles.itemContainer, { backgroundColor: 'white', alignItems:'center' }]}>
            <Image source={{uri:`${item.link}`}}

                                style={{width:'100%', height:'70%'}}
                            />
          <Text style={{color:'black'}}>{item.Pname}</Text>
          <Text style={{color:'red'}}>{item.Pprice.toFixed(2)} AZN</Text>
            {/*<TouchableHighlight>
            <Icon name="add-circle-outline"  size={30} color='green' onPress={() => {basketim.addItem(item);setCt(ct+1)}}/>
            </TouchableHighlight>*/}
            <Button color='green' on onPress={() => {basketim.addItem(item);setCt(ct+1)}}>Add</Button>
        </View>
        </ScrollView>
      )}
      renderSectionHeader={({ section }) => (
        <Text style={styles.sectionHeader}>{section.title}</Text>
      )}
    />
  );
}

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 250,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: 'whitesmoke',
  },
  sectionHeader: {
    flex: 1,
    fontSize: 20,
    fontWeight:'bolder',
    marginStart:5,
    fontWeight: '600',
    alignItems: 'center',
    backgroundColor: '#f3f3f3f3',
    color: 'black',
    padding: 10,
  },
});