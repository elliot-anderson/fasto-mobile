class FancyClass {
    constructor() {
        if(FancyClass.instance == null){
            this.basket_items = []
            FancyClass.instance = this
        }
         return FancyClass.instance
    }
    
    addItem(item){
        this.basket_items.push(item)
    }

    basketItems(){
        return this.basket_items
    }
    removeAll(){
        this.basket_items = []
    }

    removeItem(item){
        this.basket_items.splice(this.basket_items.indexOf(item), 1)
    }
}

class NotificationStatus{
    constructor(){
        if(NotificationStatus.instance == null){
            this.statusMessage = []
            NotificationStatus.instance = this
        }
        return NotificationStatus.instance
    }
    updateStatus(message){
        this.statusMessage.push(message)
    }
    clearNotifications(){
        this.statusMessage = []
    }

}
export const nStatus = new NotificationStatus()
const basketim = new FancyClass()
export default basketim

