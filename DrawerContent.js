import React from 'react';
import { View, StyleSheet, BackHandler } from 'react-native';
import {
    Avatar,
    Title,
    Caption,
    Drawer
} from 'react-native-paper';
import {
    DrawerContentScrollView,
    DrawerItem
} from '@react-navigation/drawer';

import Icon from 'react-native-vector-icons/Ionicons'


export function DrawerContent(props) {



    return(
        <View style={{flex:1}}>
            <DrawerContentScrollView {...props}>
                <View style={styles.drawerContent}>
                    <View style={styles.userInfoSection}>
                        <View style={{flexDirection:'row',marginTop: 15}}>
                            <Avatar.Image 
                                source={{
                                    uri: 'https://scontent.fgyd3-1.fna.fbcdn.net/v/t31.0-8/29873037_441977189571982_6189244213030729710_o.jpg?_nc_cat=101&_nc_sid=09cbfe&_nc_ohc=QNkG3BI31BIAX_0Um0p&_nc_ht=scontent.fgyd3-1.fna&oh=9815530178d5cd781128ce0ee1b0f901&oe=5F697B95'
                                }}
                                size={50}
                            />
                            <View style={{marginLeft:15, flexDirection:'column'}}>
                                <Title style={styles.title}>Jamal Muradov</Title>
                                <Caption style={styles.caption}>@camal1muradov</Caption>
                            </View>
                        </View>

                       
                    </View>

                    <Drawer.Section style={styles.drawerSection}>
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="home-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Main"
                            onPress={() => {props.navigation.navigate('Main')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="chatbubbles-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="FAQ"
                            onPress={() => {props.navigation.navigate('FAQ')}}
                        />
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="paper-plane-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Contact us"
                            onPress={() => {props.navigation.navigate('Contact us')}}
                        />
              
                        <DrawerItem 
                            icon={({color, size}) => (
                                <Icon 
                                name="exit-outline" 
                                color={color}
                                size={size}
                                />
                            )}
                            label="Exit"
                            onPress={() => BackHandler.exitApp()}
                        />
                    </Drawer.Section>
                    
                </View>
            </DrawerContentScrollView>
   
        </View>
    );
}

const styles = StyleSheet.create({
    drawerContent: {
      flex: 1,
    },
    userInfoSection: {
      paddingLeft: 20,
    },
    title: {
      fontSize: 16,
      marginTop: 3,
      fontWeight: 'bold',
    },
    caption: {
      fontSize: 14,
      lineHeight: 14,
    },
    row: {
      marginTop: 20,
      flexDirection: 'row',
      alignItems: 'center',
    },
    section: {
      flexDirection: 'row',
      alignItems: 'center',
      marginRight: 15,
    },
    paragraph: {
      fontWeight: 'bold',
      marginRight: 3,
    },
    drawerSection: {
      marginTop: 15,
    },
    bottomDrawerSection: {
        marginBottom: 15,
        borderTopColor: '#f4f4f4',
        borderTopWidth: 1
    },
    
  });