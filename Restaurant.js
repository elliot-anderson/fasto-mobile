import React, { Component } from 'react'
import {
  StyleSheet,
  Dimensions,

} from 'react-native'

import MapView, {
  MAP_TYPES,
  Marker,
  PROVIDER_GOOGLE
} from 'react-native-maps'



const { width, height } = Dimensions.get('window')

const ASPECT_RATIO = width / height
const LATITUDE = 40.45
const LONGITUDE = 50.09
const LATITUDE_DELTA = 0.25
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO
let id = 0

export default class Maps extends Component {
  constructor(props) {
    super(props)
    this.state = {
      region: {
        latitude: LATITUDE,
        longitude: LONGITUDE,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      },
      data:[],
     
    }
  }
  componentDidMount(){
   
        fetch('https://donerweb.herokuapp.com/doners')
        .then(item => item.json())
       .then(item =>
       this.setState({data: item}))


}
  
render(){
 

    return (

        <MapView
          style={styles.map}
          mapType={MAP_TYPES.STANDARD}
          initialRegion={this.state.region}
          provider={PROVIDER_GOOGLE}
          zoomControlEnabled={true}
          zoomEnabled={true}
          zoomTapEnabled={true}
          
        >
          {this.state.data.map(item =>   <Marker key={Math.random()} coordinate={{latitude:item.latitude,longitude:item.longitude}} title={item.dp_name} />
)}
        </MapView>



    )
  }
}



const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  map: {
    ...StyleSheet.absoluteFillObject
  },
  bubble: {
    backgroundColor: 'rgba(255,255,255,0.7)',
    paddingHorizontal: 18,
    paddingVertical: 12,
    borderRadius: 20
  },
  latlng: {
    width: 200,
    alignItems: 'stretch'
  },
  button: {
    width: 80,
    paddingHorizontal: 12,
    alignItems: 'center',
    marginHorizontal: 10
  },
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 20,
    backgroundColor: 'transparent'
  }
})