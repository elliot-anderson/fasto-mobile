import React, {useState, useEffect} from 'react';
import { StyleSheet, View, Text, Image, ScrollView, TouchableHighlight, Button } from 'react-native';
import { SectionGrid } from 'react-native-super-grid';
import basketim, {nStatus} from './fancyClass.js'
import Geolocation from '@react-native-community/geolocation'
import axios from 'axios'
import PushNotification from 'react-native-push-notification'


PushNotification.configure({
  onNotification: function (notification) {
    console.log("NOTIFICATION:", notification);
    notification.finish();
  },
    onAction: function (notification) {
    console.log("ACTION:", notification.action);
    console.log("NOTIFICATION:", notification);

  },
  
  permissions: {
    alert: true,
    badge: true,
    sound: true,
  },
  popInitialNotification: true,
  requestPermissions: true,
});

export default function Example({navigation, ct,setCt, items}) {
  const Received = () => {
    PushNotification.localNotification({
      title:'Tofiq dayı',
      message:'Sizin sifariş qəbul edildi!'
    })
    nStatus.updateStatus('Sizin sifariş qəbul edildi!')
  }
  const Preparing = () => {
    PushNotification.localNotification({
      title:'Tofiq dayı',
      message:`Sizin sifariş kuriyere ötürüldü!`
    })
    nStatus.updateStatus(`Sizin sifariş kuriyere ötürüldü!`)
  }
  const Ready = () => {
    PushNotification.localNotification({
      title:'Tofiq dayı',
      message:'Sizin sifariş çatdırıldı!'
    })
    nStatus.updateStatus('Sizin sifariş çatdırıldı!')
  }
  const Remove = () => {
    PushNotification.removeAllDeliveredNotifications();

  }
  const [location,setLoc] = useState()
  useEffect(() => {Geolocation.getCurrentPosition(
    (position) => {
        setLoc(String(position.coords.latitude) + ',' + String(position.coords.longitude))
    },
    (error) => {
        // See error code charts below.
        console.log(error.code, error.message);
    },
    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
  )},[location]
  )
  const makeOrder = () => {
    
  axios.post('https://donerweb.herokuapp.com/orders',{
      'location':location,
      'orderlist':basketim.basketItems()
    }).then(basketim.removeAll())
    .then(alert('Order\'s been sent'))
    .then(setTimeout(Received, 10000))
    .then(setTimeout(Preparing, 15000))
    .then(setTimeout(Ready, 25000))
  
  }
  var totalPrice = 0
  items.map(item => totalPrice+=item.Pprice)

  return (
    
    <SectionGrid
      itemDimension={250}
      //staticDimension={200}
      // fixed
      spacing={10}
      sections={[
        {
          title: 'Səbətdəkilər',
          data: items,
        },
       
      ]}
      style={styles.gridView}
      renderItem={({ item, section, index }) => (
        <ScrollView>
          {item.link?
        <View style={[styles.itemContainer, { backgroundColor: 'white' }]}>
            <Image source={{uri:`${item.link}`}}

                                style={{width:'100%', height:'70%'}}
                            />
          <Text style={{color:'black'}}>{item.Pname?item.Pname:''}</Text>
          <Text style={{color:'red'}}>{item.Pprice?item.Pprice.toFixed(2):''} AZN</Text>
          <TouchableHighlight 
               style={{alignItems:'center'}}>
            <Button  color='red' onPress={() =>{basketim.removeItem(item);setCt(ct*2)}} size={30} title='Remove' />
          </TouchableHighlight> 
        </View>:<View></View>}
        </ScrollView>
      )}
      renderSectionHeader={({ section }) => (
        <Text style={styles.sectionHeader}>{section.title}</Text>
      )}
      renderSectionFooter={({section}) => (
        basketim.basket_items.length == 0?<View></View>:<View style={styles.fixToText}>
          <Text style={{fontSize:16,fontWeight:'bold', color:'black'}}>Ümumi məbləğ:</Text>
          <Text style={{fontSize:24, fontWeight:'bold', color:'red'}}>{totalPrice.toFixed(2)} AZN</Text>
       <Button title="Make payment" onPress={makeOrder} />
        </View>
      )}
    />
  );
}

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 250,
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemCode: {
    fontWeight: '600',
    fontSize: 12,
    color: 'whitesmoke',
  },
  sectionHeader: {
    flex: 1,
    fontSize: 20,
    fontWeight:'bolder',
    marginStart:5,
    fontWeight: '600',
    alignItems: 'center',
    backgroundColor: '#f3f3f3f3',
    color: 'black',
    padding: 10,
  },
  fixToText: {
    justifyContent: 'space-between',
    alignItems:'center',
    fontSize:24,
  },
});