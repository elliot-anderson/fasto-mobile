import {View, Text, StyleSheet, TextInput} from 'react-native'
import React from 'react'
export default function Notifications({status}) {

    return (
      <View style={{backgroundColor:'white', flex:1}}>
        {status.length!==0?<View>{status.map(item => <TextInput style={styles.input} placeholderTextColor='black' editable={false} placeholder={item} key={Math.random()}/>)}</View>:<View style={{ top:'1100%',alignItems: 'center', justifyContent: 'center'}}><Text style={{color:'gray'}}>No new notifications</Text></View>}
       
    </View>
    );
  }
  const styles = StyleSheet.create({
    container: {
       paddingTop: 23
    },
    input: {
      margin:5,
       height: 70,
       backgroundColor:'whitesmoke',
       borderColor:'whitesmoke',
       borderWidth: 16,
       fontSize:16,
       fontWeight:'bold',
       flexDirection:'row'
    }
  })